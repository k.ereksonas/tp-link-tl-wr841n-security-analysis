# TP Link TL-WR841N router cybersecurity analysis

# Prerequisite

After testing the security of my IP camera I've decided to do an overview of the security of my router.

# Plan of Analysis

1. Gathering of technical information about the router.
2. Find firmware and installed software.
3. Check for known exploits.
4. Check the severity of found exploits.
5. Present the ways of how to mitigate the risks.
6. Conclusion.

# Technical Information

In this section I am presenting the technical information that I have gathered about tested router.

## Open Ports

In this subsection I am presenting the results of a port scan.

### TCP Port Scan

TCP port scan results are presented.

### UDP Port Scan

UDP port scan results are presented.

## OS Detection

OS detection scan.
